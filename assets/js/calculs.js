jq(function(){

/*Ci-dessus fonction JS qui permet de s'assurer que le jQ est bien chargé, malgré que source de jQ via CDN*/
    

  var factosommiel = 4;


  $('#valeur').html(factosommiel);
    
    console.log('Calculs effectués sur ' + factosommiel);
    $('#sommiel').html(factosommiel);
    
    setTimeout(function () {
      var t = 0, // total
      c = 1;     // compteur
      
      while (c <= factosommiel) {
        t += c;
        c++;
      }
      
      function factoriel(n){
        if(n==0) return 1;
        else return factoriel (n-1)*n
      }
      
      var loto = factoriel(49)/(factoriel(6) * factoriel(43));
  
      // r= range(0,2); // 2doli dev cette fctn en jQ
      // console.log(r);
      
      $('.lionel').css('color', 'cyan').html('Sommiel de ' + factosommiel + ' = ' + t + '<br>' + 'Factoriel de ' + factosommiel + ' = ' + number_format(factoriel(factosommiel), 0, '.', ' ') + '<hr>Nombre de combinaisons du loto: ' + number_format(loto, 0, ',', ' '));
  
    }, (factosommiel*500)); // Tempo: 1/2 seconde par unité
    
    function number_format (number, decimals, dec_point, thousands_sep) {
      number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
      var n = !isFinite(+number) ? 0 : +number,
          prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
          sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
          dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
          s = '',
          toFixedFix = function (n, prec) {
              var k = Math.pow(10, prec);
              return '' + Math.round(n * k) / k;
          };
      // Fix for IE parseFloat(0.55).toFixed(0) = 0;
      s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
      if (s[0].length > 3) {
          s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
      }
      if ((s[1] || '').length < prec) {
          s[1] = s[1] || '';
          s[1] += new Array(prec - s[1].length + 1).join('0');
      }
      return s.join(dec);
    }
  
});