<?php
$titre = $_GET['page'];

if (preg_match('/\//', $titre)) {
  $titres= explode('/', $titre);
  $titre = array_pop($titres);
}

?>
  
  <head>
    <meta charset="utf-8">
    <title><?=ucWords($titre)?> | Sitetest</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Un site juste pour s'entraîner (Git, HTML, CSS, JS, PHP)...">
    <meta name="keywords" content="Programmation, Git, UNgit, c57.fr">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="icon" type="image/gif" href="/favicon.ico">

    <?php
      include "assets/css/all.php";
    ?>
    
    <script>
      function jq(fn){var d=document;(d.readyState=='loading')?d.addEventListener('DOMContentLoaded',fn):fn();}
    </script>
  </head>
